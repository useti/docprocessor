// CDebug.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ProcessingDLL.h"
#include <string>

extern "C" __declspec(dllimport)
int SubstractTemplate(const char* settings_c_str);

int main(int argc, char* argv[])
{
	//std::string input_file = static_cast<char*>("..\\Tests\\TestData\\Document_0.5x_afine.png");
	//std::string output_file = static_cast<char*>("..\\Tests\\TestData\\output.png");
	//std::string template_file = static_cast<char*>("..\\Tests\\TestData\\Template_0.5x.png");

	std::string settings_string = static_cast<char*>("{ \"INPUT_FILE\":\"..\\\\Tests\\\\TestData\\\\Couch_photo.JPG\", \
														\"OUTPUT_FILE\":\"..\\\\Tests\\\\TestData\\\\output.png\",\
														\"TEMPLATE_FILE\":\"..\\\\Tests\\\\TestData\\\\Couch_template.png\",\
														\"PREPROCESSINGS\": 17,\
														\"BINARISATION_TRH\": 127,\
														\"BINARISATION_MAX\": 255,\
														\"BLUR_KERNEL_W\": 5,\
														\"BLUR_KERNEL_H\": 5,\
														\"ADAPTIVE_TRH_MAX\": 255,\
														\"ADAPTIVE_TRH_BLOCK\": 3,\
														\"ADAPTIVE_TRH_C\": 5,\
														\"KUWAHARA_KERNEL\": 7,\
														\"MIN_HESSIAN\": 400, \
														\"PYR_NOIZE_TRH\": 64, \
														\"PYR_LEVEL\": 1, \
														\"PYR_MIN\": 0, \
														\"PYR_MAX\": 255, \
														\"PYR_K\": 2 \
														}");


	unsigned char prepr = PR_DLL_PREPR_TRHS | PR_DLL_PREPR_BLUR ;
	//unsigned char prepr = PR_DLL_PREPR_NOPR;
	SubstractTemplate(settings_string.c_str());
	return 0;
}

