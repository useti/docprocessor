﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using ProcessingWrap;

namespace Tests
{
    [TestClass]
    public class ProcessingTests
    {
        [TestMethod]
        public void LoadClasses()
        {
            var processing = new Processing();
            Assert.AreEqual(true, true);
        }

        [TestMethod]
        public void TestProcessImages()
        {
            var processing = new Processing();
            String process_string = "{ \"INPUT_FILE\":\"..\\\\..\\\\..\\\\TestData\\\\Document_0.5x_afine.png\", " +
									  "\"OUTPUT_FILE\":\"..\\\\..\\\\..\\\\TestData\\\\output.png\", " +
                                      "\"TEMPLATE_FILE\":\"..\\\\..\\\\..\\\\TestData\\\\Template_0.5x.png\", " +
									  "\"PREPROCESSINGS\": 17, " +
									  "\"BINARISATION_TRH\": 127, " +
									  "\"BINARISATION_MAX\": 255, " +
									  "\"BLUR_KERNEL_W\": 3,"+
									  "\"BLUR_KERNEL_H\": 3, "+
									  "\"ADAPTIVE_TRH_MAX\": 255,"+
									  "\"ADAPTIVE_TRH_BLOCK\": 3, "+
									  "\"ADAPTIVE_TRH_C\": 5," +
									  "\"KUWAHARA_KERNEL\": 7, " +
									  "\"MIN_HESSIAN\": 400, "+
                                      "\"PYR_NOIZE_TRH\": 127, " +
									  "\"PYR_LEVEL\": 1, "+
									  "\"PYR_MIN\": 0, "+
									  "\"PYR_MAX\": 255,"+
									  "\"PYR_K\": 2 "+
                                      " }";
            int ret = processing.ProcessImages(process_string);

            Assert.AreEqual(0, ret);
        }

    }
}
