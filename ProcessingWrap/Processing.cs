﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ProcessingWrap
{
    public class Processing
    {

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDllDirectory(string lpPathName);

        [DllImport(@"Processing.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]        
        public static extern int SubstractTemplate(String settings_string);

        public bool SetDDirectory(string dir)
        {
            return SetDllDirectory(dir);
        }

        public int ProcessImages(String settings_string)
        {
            int ret = 0;
            try
            {
                ret = SubstractTemplate(settings_string);
            }
            catch (Exception ex)
            {
                String exc = ex.Message;
                return -1;
            }
            return ret;
        }
    }

    public enum Preprocessings
    {
        //#define PR_DLL_PREPR_NOPR 0
        NO_PREPROCESS = 0,
        //#define PR_DLL_PREPR_BLUR 1
        BLUR_IMG = 1,
        //#define PR_DLL_PREPR_KUWH 2
        KUWAHARA_BLUR = 2,
        //#define PR_DLL_PREPR_EQHS 4
        EQUALIZE_HISTOGRAMM = 4,
        //#define PR_DLL_PREPR_ATRH 8
        ADAPTIVE_THRESOLD = 8,
        //#define PR_DLL_PREPR_TRHS 16
        THRESOLD = 16,
        //#define PR_DLL_PREPR_APYR 32
        PYRAMIDAL_ADAPTIVE = 32
    }
}
