﻿namespace TestApp
{
    partial class Case
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chlbPreprocessings = new System.Windows.Forms.CheckedListBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PYR_K = new System.Windows.Forms.TextBox();
            this.PYR_MAX = new System.Windows.Forms.TextBox();
            this.PYR_MIN = new System.Windows.Forms.TextBox();
            this.PYR_LEVEL = new System.Windows.Forms.TextBox();
            this.PYR_NOIZE_TRH = new System.Windows.Forms.TextBox();
            this.MIN_HESSIAN = new System.Windows.Forms.TextBox();
            this.KUWAHARA_KERNEL = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_C = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_BLOCK = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_MAX = new System.Windows.Forms.TextBox();
            this.BLUR_KERNEL_H = new System.Windows.Forms.TextBox();
            this.BLUR_KERNEL_W = new System.Windows.Forms.TextBox();
            this.BINARISATION_MAX = new System.Windows.Forms.TextBox();
            this.BINARISATION_TRH = new System.Windows.Forms.TextBox();
            this.btnSavePath = new System.Windows.Forms.Button();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOpenDoc = new System.Windows.Forms.Button();
            this.tbDocument = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOpenTemplate = new System.Windows.Forms.Button();
            this.tbTemplate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CASE_NAME = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pbTemplate = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pbDocument = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pbOutput = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTemplate)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDocument)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbOutput)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chlbPreprocessings);
            this.splitContainer1.Panel1.Controls.Add(this.btnProcess);
            this.splitContainer1.Panel1.Controls.Add(this.btnSave);
            this.splitContainer1.Panel1.Controls.Add(this.label14);
            this.splitContainer1.Panel1.Controls.Add(this.label13);
            this.splitContainer1.Panel1.Controls.Add(this.label12);
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.label10);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label15);
            this.splitContainer1.Panel1.Controls.Add(this.label16);
            this.splitContainer1.Panel1.Controls.Add(this.label17);
            this.splitContainer1.Panel1.Controls.Add(this.label18);
            this.splitContainer1.Panel1.Controls.Add(this.PYR_K);
            this.splitContainer1.Panel1.Controls.Add(this.PYR_MAX);
            this.splitContainer1.Panel1.Controls.Add(this.PYR_MIN);
            this.splitContainer1.Panel1.Controls.Add(this.PYR_LEVEL);
            this.splitContainer1.Panel1.Controls.Add(this.PYR_NOIZE_TRH);
            this.splitContainer1.Panel1.Controls.Add(this.MIN_HESSIAN);
            this.splitContainer1.Panel1.Controls.Add(this.KUWAHARA_KERNEL);
            this.splitContainer1.Panel1.Controls.Add(this.ADAPTIVE_TRH_C);
            this.splitContainer1.Panel1.Controls.Add(this.ADAPTIVE_TRH_BLOCK);
            this.splitContainer1.Panel1.Controls.Add(this.ADAPTIVE_TRH_MAX);
            this.splitContainer1.Panel1.Controls.Add(this.BLUR_KERNEL_H);
            this.splitContainer1.Panel1.Controls.Add(this.BLUR_KERNEL_W);
            this.splitContainer1.Panel1.Controls.Add(this.BINARISATION_MAX);
            this.splitContainer1.Panel1.Controls.Add(this.BINARISATION_TRH);
            this.splitContainer1.Panel1.Controls.Add(this.btnSavePath);
            this.splitContainer1.Panel1.Controls.Add(this.tbOutput);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.btnOpenDoc);
            this.splitContainer1.Panel1.Controls.Add(this.tbDocument);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.btnOpenTemplate);
            this.splitContainer1.Panel1.Controls.Add(this.tbTemplate);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.CASE_NAME);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(965, 750);
            this.splitContainer1.SplitterDistance = 254;
            this.splitContainer1.TabIndex = 0;
            // 
            // chlbPreprocessings
            // 
            this.chlbPreprocessings.FormattingEnabled = true;
            this.chlbPreprocessings.Location = new System.Drawing.Point(15, 167);
            this.chlbPreprocessings.Name = "chlbPreprocessings";
            this.chlbPreprocessings.Size = new System.Drawing.Size(236, 154);
            this.chlbPreprocessings.TabIndex = 58;
            this.chlbPreprocessings.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chlbPreprocessings_ItemCheck);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(97, 711);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 57;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 711);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 56;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(103, 688);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "PYR_K";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(87, 664);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 54;
            this.label13.Text = "PYR_MAX";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(90, 637);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 53;
            this.label12.Text = "PYR_MIN";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(77, 610);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 52;
            this.label11.Text = "PYR_LEVEL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(48, 583);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "PYR_NOIZE_TRH";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 556);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 50;
            this.label9.Text = "MIN_HESSIAN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 529);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 49;
            this.label8.Text = "KUWAHARA_KERNEL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 502);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "ADAPTIVE_TRH_C";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 475);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "ADAPTIVE_TRH_BLOCK";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 448);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "ADAPTIVE_TRH_MAX";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(46, 421);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "BLUR_KERNEL_H";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(43, 394);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "BLUR_KERNEL_W";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 368);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "BINARISATION_MAX";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(33, 342);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "BINARISATION_TRH";
            // 
            // PYR_K
            // 
            this.PYR_K.Location = new System.Drawing.Point(151, 688);
            this.PYR_K.Name = "PYR_K";
            this.PYR_K.Size = new System.Drawing.Size(100, 20);
            this.PYR_K.TabIndex = 41;
            this.PYR_K.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // PYR_MAX
            // 
            this.PYR_MAX.Location = new System.Drawing.Point(151, 661);
            this.PYR_MAX.Name = "PYR_MAX";
            this.PYR_MAX.Size = new System.Drawing.Size(100, 20);
            this.PYR_MAX.TabIndex = 40;
            this.PYR_MAX.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // PYR_MIN
            // 
            this.PYR_MIN.Location = new System.Drawing.Point(151, 634);
            this.PYR_MIN.Name = "PYR_MIN";
            this.PYR_MIN.Size = new System.Drawing.Size(100, 20);
            this.PYR_MIN.TabIndex = 39;
            this.PYR_MIN.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // PYR_LEVEL
            // 
            this.PYR_LEVEL.Location = new System.Drawing.Point(151, 607);
            this.PYR_LEVEL.Name = "PYR_LEVEL";
            this.PYR_LEVEL.Size = new System.Drawing.Size(100, 20);
            this.PYR_LEVEL.TabIndex = 38;
            this.PYR_LEVEL.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // PYR_NOIZE_TRH
            // 
            this.PYR_NOIZE_TRH.Location = new System.Drawing.Point(151, 580);
            this.PYR_NOIZE_TRH.Name = "PYR_NOIZE_TRH";
            this.PYR_NOIZE_TRH.Size = new System.Drawing.Size(100, 20);
            this.PYR_NOIZE_TRH.TabIndex = 37;
            this.PYR_NOIZE_TRH.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // MIN_HESSIAN
            // 
            this.MIN_HESSIAN.Location = new System.Drawing.Point(151, 553);
            this.MIN_HESSIAN.Name = "MIN_HESSIAN";
            this.MIN_HESSIAN.Size = new System.Drawing.Size(100, 20);
            this.MIN_HESSIAN.TabIndex = 36;
            this.MIN_HESSIAN.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // KUWAHARA_KERNEL
            // 
            this.KUWAHARA_KERNEL.Location = new System.Drawing.Point(151, 526);
            this.KUWAHARA_KERNEL.Name = "KUWAHARA_KERNEL";
            this.KUWAHARA_KERNEL.Size = new System.Drawing.Size(100, 20);
            this.KUWAHARA_KERNEL.TabIndex = 35;
            this.KUWAHARA_KERNEL.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // ADAPTIVE_TRH_C
            // 
            this.ADAPTIVE_TRH_C.Location = new System.Drawing.Point(151, 499);
            this.ADAPTIVE_TRH_C.Name = "ADAPTIVE_TRH_C";
            this.ADAPTIVE_TRH_C.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_C.TabIndex = 34;
            this.ADAPTIVE_TRH_C.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // ADAPTIVE_TRH_BLOCK
            // 
            this.ADAPTIVE_TRH_BLOCK.Location = new System.Drawing.Point(151, 472);
            this.ADAPTIVE_TRH_BLOCK.Name = "ADAPTIVE_TRH_BLOCK";
            this.ADAPTIVE_TRH_BLOCK.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_BLOCK.TabIndex = 33;
            this.ADAPTIVE_TRH_BLOCK.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // ADAPTIVE_TRH_MAX
            // 
            this.ADAPTIVE_TRH_MAX.Location = new System.Drawing.Point(151, 445);
            this.ADAPTIVE_TRH_MAX.Name = "ADAPTIVE_TRH_MAX";
            this.ADAPTIVE_TRH_MAX.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_MAX.TabIndex = 32;
            this.ADAPTIVE_TRH_MAX.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // BLUR_KERNEL_H
            // 
            this.BLUR_KERNEL_H.Location = new System.Drawing.Point(151, 418);
            this.BLUR_KERNEL_H.Name = "BLUR_KERNEL_H";
            this.BLUR_KERNEL_H.Size = new System.Drawing.Size(100, 20);
            this.BLUR_KERNEL_H.TabIndex = 31;
            this.BLUR_KERNEL_H.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // BLUR_KERNEL_W
            // 
            this.BLUR_KERNEL_W.Location = new System.Drawing.Point(151, 391);
            this.BLUR_KERNEL_W.Name = "BLUR_KERNEL_W";
            this.BLUR_KERNEL_W.Size = new System.Drawing.Size(100, 20);
            this.BLUR_KERNEL_W.TabIndex = 30;
            this.BLUR_KERNEL_W.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // BINARISATION_MAX
            // 
            this.BINARISATION_MAX.Location = new System.Drawing.Point(151, 365);
            this.BINARISATION_MAX.Name = "BINARISATION_MAX";
            this.BINARISATION_MAX.Size = new System.Drawing.Size(100, 20);
            this.BINARISATION_MAX.TabIndex = 29;
            this.BINARISATION_MAX.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // BINARISATION_TRH
            // 
            this.BINARISATION_TRH.Location = new System.Drawing.Point(151, 339);
            this.BINARISATION_TRH.Name = "BINARISATION_TRH";
            this.BINARISATION_TRH.Size = new System.Drawing.Size(100, 20);
            this.BINARISATION_TRH.TabIndex = 28;
            this.BINARISATION_TRH.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // btnSavePath
            // 
            this.btnSavePath.Location = new System.Drawing.Point(217, 138);
            this.btnSavePath.Name = "btnSavePath";
            this.btnSavePath.Size = new System.Drawing.Size(34, 23);
            this.btnSavePath.TabIndex = 10;
            this.btnSavePath.Text = "...";
            this.btnSavePath.UseVisualStyleBackColor = true;
            this.btnSavePath.Click += new System.EventHandler(this.btnSavePath_Click);
            // 
            // tbOutput
            // 
            this.tbOutput.Location = new System.Drawing.Point(15, 140);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(196, 20);
            this.tbOutput.TabIndex = 9;
            this.tbOutput.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Destination file:";
            // 
            // btnOpenDoc
            // 
            this.btnOpenDoc.Location = new System.Drawing.Point(217, 99);
            this.btnOpenDoc.Name = "btnOpenDoc";
            this.btnOpenDoc.Size = new System.Drawing.Size(34, 23);
            this.btnOpenDoc.TabIndex = 7;
            this.btnOpenDoc.Text = "...";
            this.btnOpenDoc.UseVisualStyleBackColor = true;
            this.btnOpenDoc.Click += new System.EventHandler(this.btnOpenDoc_Click);
            // 
            // tbDocument
            // 
            this.tbDocument.Location = new System.Drawing.Point(15, 101);
            this.tbDocument.Name = "tbDocument";
            this.tbDocument.Size = new System.Drawing.Size(196, 20);
            this.tbDocument.TabIndex = 6;
            this.tbDocument.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Source file:";
            // 
            // btnOpenTemplate
            // 
            this.btnOpenTemplate.Location = new System.Drawing.Point(217, 59);
            this.btnOpenTemplate.Name = "btnOpenTemplate";
            this.btnOpenTemplate.Size = new System.Drawing.Size(34, 23);
            this.btnOpenTemplate.TabIndex = 4;
            this.btnOpenTemplate.Text = "...";
            this.btnOpenTemplate.UseVisualStyleBackColor = true;
            this.btnOpenTemplate.Click += new System.EventHandler(this.btnOpenTemplate_Click);
            // 
            // tbTemplate
            // 
            this.tbTemplate.Location = new System.Drawing.Point(15, 62);
            this.tbTemplate.Name = "tbTemplate";
            this.tbTemplate.Size = new System.Drawing.Size(196, 20);
            this.tbTemplate.TabIndex = 3;
            this.tbTemplate.TextChanged += new System.EventHandler(this.tbTemplate_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Template file:";
            // 
            // CASE_NAME
            // 
            this.CASE_NAME.Location = new System.Drawing.Point(15, 23);
            this.CASE_NAME.Name = "CASE_NAME";
            this.CASE_NAME.Size = new System.Drawing.Size(236, 20);
            this.CASE_NAME.TabIndex = 1;
            this.CASE_NAME.TextChanged += new System.EventHandler(this.CASE_NAME_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Case Name:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(707, 750);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pbTemplate);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(699, 724);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Template";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pbTemplate
            // 
            this.pbTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbTemplate.Location = new System.Drawing.Point(3, 3);
            this.pbTemplate.Name = "pbTemplate";
            this.pbTemplate.Size = new System.Drawing.Size(693, 718);
            this.pbTemplate.TabIndex = 0;
            this.pbTemplate.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pbDocument);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(699, 724);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Source document";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pbDocument
            // 
            this.pbDocument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbDocument.Location = new System.Drawing.Point(3, 3);
            this.pbDocument.Name = "pbDocument";
            this.pbDocument.Size = new System.Drawing.Size(693, 718);
            this.pbDocument.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDocument.TabIndex = 0;
            this.pbDocument.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pbOutput);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(699, 724);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Processed document";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pbOutput
            // 
            this.pbOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbOutput.Location = new System.Drawing.Point(3, 3);
            this.pbOutput.Name = "pbOutput";
            this.pbOutput.Size = new System.Drawing.Size(693, 718);
            this.pbOutput.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbOutput.TabIndex = 0;
            this.pbOutput.TabStop = false;
            // 
            // Case
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 750);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Case";
            this.Text = "Case";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbTemplate)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbDocument)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbOutput)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox CASE_NAME;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox tbOutput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOpenDoc;
        private System.Windows.Forms.TextBox tbDocument;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOpenTemplate;
        private System.Windows.Forms.TextBox tbTemplate;
        private System.Windows.Forms.Button btnSavePath;
        private System.Windows.Forms.TextBox PYR_K;
        private System.Windows.Forms.TextBox PYR_MAX;
        private System.Windows.Forms.TextBox PYR_MIN;
        private System.Windows.Forms.TextBox PYR_LEVEL;
        private System.Windows.Forms.TextBox PYR_NOIZE_TRH;
        private System.Windows.Forms.TextBox MIN_HESSIAN;
        private System.Windows.Forms.TextBox KUWAHARA_KERNEL;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_C;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_BLOCK;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_MAX;
        private System.Windows.Forms.TextBox BLUR_KERNEL_H;
        private System.Windows.Forms.TextBox BLUR_KERNEL_W;
        private System.Windows.Forms.TextBox BINARISATION_MAX;
        private System.Windows.Forms.TextBox BINARISATION_TRH;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckedListBox chlbPreprocessings;
        private System.Windows.Forms.PictureBox pbTemplate;
        private System.Windows.Forms.PictureBox pbDocument;
        private System.Windows.Forms.PictureBox pbOutput;
    }
}