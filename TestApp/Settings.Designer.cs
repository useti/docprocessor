﻿namespace TestApp
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.BINARISATION_TRH = new System.Windows.Forms.TextBox();
            this.BINARISATION_MAX = new System.Windows.Forms.TextBox();
            this.BLUR_KERNEL_W = new System.Windows.Forms.TextBox();
            this.BLUR_KERNEL_H = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_MAX = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_BLOCK = new System.Windows.Forms.TextBox();
            this.ADAPTIVE_TRH_C = new System.Windows.Forms.TextBox();
            this.KUWAHARA_KERNEL = new System.Windows.Forms.TextBox();
            this.MIN_HESSIAN = new System.Windows.Forms.TextBox();
            this.PYR_NOIZE_TRH = new System.Windows.Forms.TextBox();
            this.PYR_LEVEL = new System.Windows.Forms.TextBox();
            this.PYR_MIN = new System.Windows.Forms.TextBox();
            this.PYR_MAX = new System.Windows.Forms.TextBox();
            this.PYR_K = new System.Windows.Forms.TextBox();
            this.chlbPreprocessings = new System.Windows.Forms.CheckedListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnChancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "BINARISATION_TRH";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(315, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "BINARISATION_MAX";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(325, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "BLUR_KERNEL_W";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "BLUR_KERNEL_H";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(309, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "ADAPTIVE_TRH_MAX";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(297, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "ADAPTIVE_TRH_BLOCK";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(325, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "ADAPTIVE_TRH_C";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(308, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "KUWAHARA_KERNEL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(347, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "MIN_HESSIAN";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(330, 257);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "PYR_NOIZE_TRH";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(359, 284);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "PYR_LEVEL";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(372, 311);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "PYR_MIN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(369, 338);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "PYR_MAX";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(385, 362);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "PYR_K";
            // 
            // BINARISATION_TRH
            // 
            this.BINARISATION_TRH.Location = new System.Drawing.Point(430, 13);
            this.BINARISATION_TRH.Name = "BINARISATION_TRH";
            this.BINARISATION_TRH.Size = new System.Drawing.Size(100, 20);
            this.BINARISATION_TRH.TabIndex = 14;
            // 
            // BINARISATION_MAX
            // 
            this.BINARISATION_MAX.Location = new System.Drawing.Point(430, 39);
            this.BINARISATION_MAX.Name = "BINARISATION_MAX";
            this.BINARISATION_MAX.Size = new System.Drawing.Size(100, 20);
            this.BINARISATION_MAX.TabIndex = 15;
            // 
            // BLUR_KERNEL_W
            // 
            this.BLUR_KERNEL_W.Location = new System.Drawing.Point(430, 65);
            this.BLUR_KERNEL_W.Name = "BLUR_KERNEL_W";
            this.BLUR_KERNEL_W.Size = new System.Drawing.Size(100, 20);
            this.BLUR_KERNEL_W.TabIndex = 16;
            // 
            // BLUR_KERNEL_H
            // 
            this.BLUR_KERNEL_H.Location = new System.Drawing.Point(430, 92);
            this.BLUR_KERNEL_H.Name = "BLUR_KERNEL_H";
            this.BLUR_KERNEL_H.Size = new System.Drawing.Size(100, 20);
            this.BLUR_KERNEL_H.TabIndex = 17;
            // 
            // ADAPTIVE_TRH_MAX
            // 
            this.ADAPTIVE_TRH_MAX.Location = new System.Drawing.Point(430, 119);
            this.ADAPTIVE_TRH_MAX.Name = "ADAPTIVE_TRH_MAX";
            this.ADAPTIVE_TRH_MAX.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_MAX.TabIndex = 18;
            // 
            // ADAPTIVE_TRH_BLOCK
            // 
            this.ADAPTIVE_TRH_BLOCK.Location = new System.Drawing.Point(430, 146);
            this.ADAPTIVE_TRH_BLOCK.Name = "ADAPTIVE_TRH_BLOCK";
            this.ADAPTIVE_TRH_BLOCK.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_BLOCK.TabIndex = 19;
            // 
            // ADAPTIVE_TRH_C
            // 
            this.ADAPTIVE_TRH_C.Location = new System.Drawing.Point(430, 173);
            this.ADAPTIVE_TRH_C.Name = "ADAPTIVE_TRH_C";
            this.ADAPTIVE_TRH_C.Size = new System.Drawing.Size(100, 20);
            this.ADAPTIVE_TRH_C.TabIndex = 20;
            // 
            // KUWAHARA_KERNEL
            // 
            this.KUWAHARA_KERNEL.Location = new System.Drawing.Point(430, 200);
            this.KUWAHARA_KERNEL.Name = "KUWAHARA_KERNEL";
            this.KUWAHARA_KERNEL.Size = new System.Drawing.Size(100, 20);
            this.KUWAHARA_KERNEL.TabIndex = 21;
            // 
            // MIN_HESSIAN
            // 
            this.MIN_HESSIAN.Location = new System.Drawing.Point(430, 227);
            this.MIN_HESSIAN.Name = "MIN_HESSIAN";
            this.MIN_HESSIAN.Size = new System.Drawing.Size(100, 20);
            this.MIN_HESSIAN.TabIndex = 22;
            // 
            // PYR_NOIZE_TRH
            // 
            this.PYR_NOIZE_TRH.Location = new System.Drawing.Point(430, 254);
            this.PYR_NOIZE_TRH.Name = "PYR_NOIZE_TRH";
            this.PYR_NOIZE_TRH.Size = new System.Drawing.Size(100, 20);
            this.PYR_NOIZE_TRH.TabIndex = 23;
            // 
            // PYR_LEVEL
            // 
            this.PYR_LEVEL.Location = new System.Drawing.Point(430, 281);
            this.PYR_LEVEL.Name = "PYR_LEVEL";
            this.PYR_LEVEL.Size = new System.Drawing.Size(100, 20);
            this.PYR_LEVEL.TabIndex = 24;
            // 
            // PYR_MIN
            // 
            this.PYR_MIN.Location = new System.Drawing.Point(430, 308);
            this.PYR_MIN.Name = "PYR_MIN";
            this.PYR_MIN.Size = new System.Drawing.Size(100, 20);
            this.PYR_MIN.TabIndex = 25;
            // 
            // PYR_MAX
            // 
            this.PYR_MAX.Location = new System.Drawing.Point(430, 335);
            this.PYR_MAX.Name = "PYR_MAX";
            this.PYR_MAX.Size = new System.Drawing.Size(100, 20);
            this.PYR_MAX.TabIndex = 26;
            // 
            // PYR_K
            // 
            this.PYR_K.Location = new System.Drawing.Point(430, 362);
            this.PYR_K.Name = "PYR_K";
            this.PYR_K.Size = new System.Drawing.Size(100, 20);
            this.PYR_K.TabIndex = 27;
            // 
            // chlbPreprocessings
            // 
            this.chlbPreprocessings.FormattingEnabled = true;
            this.chlbPreprocessings.Location = new System.Drawing.Point(16, 31);
            this.chlbPreprocessings.Name = "chlbPreprocessings";
            this.chlbPreprocessings.Size = new System.Drawing.Size(271, 154);
            this.chlbPreprocessings.TabIndex = 28;
            this.chlbPreprocessings.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chlbPreprocessings_ItemCheck);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Prepcocessings";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(16, 426);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnChancel
            // 
            this.btnChancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnChancel.Location = new System.Drawing.Point(98, 426);
            this.btnChancel.Name = "btnChancel";
            this.btnChancel.Size = new System.Drawing.Size(75, 23);
            this.btnChancel.TabIndex = 31;
            this.btnChancel.Text = "Chancel";
            this.btnChancel.UseVisualStyleBackColor = true;
            this.btnChancel.Click += new System.EventHandler(this.btnChancel_Click);
            // 
            // Settings
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnChancel;
            this.ClientSize = new System.Drawing.Size(547, 461);
            this.Controls.Add(this.btnChancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.chlbPreprocessings);
            this.Controls.Add(this.PYR_K);
            this.Controls.Add(this.PYR_MAX);
            this.Controls.Add(this.PYR_MIN);
            this.Controls.Add(this.PYR_LEVEL);
            this.Controls.Add(this.PYR_NOIZE_TRH);
            this.Controls.Add(this.MIN_HESSIAN);
            this.Controls.Add(this.KUWAHARA_KERNEL);
            this.Controls.Add(this.ADAPTIVE_TRH_C);
            this.Controls.Add(this.ADAPTIVE_TRH_BLOCK);
            this.Controls.Add(this.ADAPTIVE_TRH_MAX);
            this.Controls.Add(this.BLUR_KERNEL_H);
            this.Controls.Add(this.BLUR_KERNEL_W);
            this.Controls.Add(this.BINARISATION_MAX);
            this.Controls.Add(this.BINARISATION_TRH);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox BINARISATION_TRH;
        private System.Windows.Forms.TextBox BINARISATION_MAX;
        private System.Windows.Forms.TextBox BLUR_KERNEL_W;
        private System.Windows.Forms.TextBox BLUR_KERNEL_H;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_MAX;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_BLOCK;
        private System.Windows.Forms.TextBox ADAPTIVE_TRH_C;
        private System.Windows.Forms.TextBox KUWAHARA_KERNEL;
        private System.Windows.Forms.TextBox MIN_HESSIAN;
        private System.Windows.Forms.TextBox PYR_NOIZE_TRH;
        private System.Windows.Forms.TextBox PYR_LEVEL;
        private System.Windows.Forms.TextBox PYR_MIN;
        private System.Windows.Forms.TextBox PYR_MAX;
        private System.Windows.Forms.TextBox PYR_K;
        private System.Windows.Forms.CheckedListBox chlbPreprocessings;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnChancel;

    }
}