﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApp
{
    public enum RunStatus
    {
        DLL_LOAD_ERROR = -1,
        PROCESS_OK     =  0,
        PROCESS_ERR		=1,
        PROCESS_LOAD_IMG_ERR =	2,
        PROCESS_CLEANUP_ERR	 =  3,
        PROCESS_RESIZE_ERR	 =  4,
        PROCESS_EXTRACT_ERR	 =  5,
        PROCESS_SAVE_IMG_ERR =	16
    }
}
