﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessingWrap;

namespace TestApp
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            FillPreprocessings();

            FillFields();
        }

        private void FillFields()
        {
            BINARISATION_TRH.Text = Properties.Settings.Default.BINARISATION_TRH.ToString();
            BINARISATION_MAX.Text = Properties.Settings.Default.BINARISATION_MAX.ToString();
            BLUR_KERNEL_W.Text = Properties.Settings.Default.BLUR_KERNEL_W.ToString();
            BLUR_KERNEL_H.Text = Properties.Settings.Default.BLUR_KERNEL_H.ToString();
            ADAPTIVE_TRH_MAX.Text = Properties.Settings.Default.ADAPTIVE_TRH_MAX.ToString();
            ADAPTIVE_TRH_BLOCK.Text = Properties.Settings.Default.ADAPTIVE_TRH_BLOCK.ToString();
            ADAPTIVE_TRH_C.Text = Properties.Settings.Default.ADAPTIVE_TRH_C.ToString();
            KUWAHARA_KERNEL.Text = Properties.Settings.Default.KUWAHARA_KERNEL.ToString();
            MIN_HESSIAN.Text = Properties.Settings.Default.MIN_HESSIAN.ToString();
            PYR_NOIZE_TRH.Text = Properties.Settings.Default.PYR_NOIZE_TRH.ToString();
            PYR_LEVEL.Text = Properties.Settings.Default.PYR_LEVEL.ToString();
            PYR_MIN.Text = Properties.Settings.Default.PYR_MIN.ToString();
            PYR_MAX.Text = Properties.Settings.Default.PYR_MAX.ToString();
            PYR_K.Text = Properties.Settings.Default.PYR_K.ToString();
        }

        private void FillPreprocessings()
        {
            chlbPreprocessings.Items.Add(Preprocessings.NO_PREPROCESS.ToString(),
                Properties.Settings.Default.NO_PREPROCESS);
            chlbPreprocessings.Items.Add(Preprocessings.BLUR_IMG.ToString(), Properties.Settings.Default.BLUR_IMG);
            chlbPreprocessings.Items.Add(Preprocessings.KUWAHARA_BLUR.ToString(),
                Properties.Settings.Default.KUWAHARA_BLUR);
            chlbPreprocessings.Items.Add(Preprocessings.EQUALIZE_HISTOGRAMM.ToString(),
                Properties.Settings.Default.EQUALIZE_HISTOGRAMM);
            chlbPreprocessings.Items.Add(Preprocessings.ADAPTIVE_THRESOLD.ToString(),
                Properties.Settings.Default.ADAPTIVE_THRESOLD);
            chlbPreprocessings.Items.Add(Preprocessings.THRESOLD.ToString(), Properties.Settings.Default.THRESOLD);
            chlbPreprocessings.Items.Add(Preprocessings.PYRAMIDAL_ADAPTIVE.ToString(),
                Properties.Settings.Default.PYRAMIDAL_ADAPTIVE);
        }

        private void chlbPreprocessings_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                for (int i = 1; i < chlbPreprocessings.Items.Count; i++)
                {
                    chlbPreprocessings.SetItemChecked(i, false);
                }
            }
        }

        private void btnChancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            applyFields();

            applyPreprocessings();

            Properties.Settings.Default.Save();

            DialogResult = DialogResult.OK;

            Close();
        }

        private void applyPreprocessings()
        {
            int preprocessings = 0;
            foreach (var chbItem in chlbPreprocessings.CheckedItems)
            {
                if (chbItem.Equals("BLUR_IMG"))
                {
                    preprocessings += (int) Preprocessings.BLUR_IMG;
                    Properties.Settings.Default.BLUR_IMG = true;
                }
                if (chbItem.Equals("KUWAHARA_BLUR"))
                {
                    preprocessings += (int) Preprocessings.KUWAHARA_BLUR;
                    Properties.Settings.Default.KUWAHARA_BLUR = true;
                }
                if (chbItem.Equals("EQUALIZE_HISTOGRAMM"))
                {
                    preprocessings += (int) Preprocessings.EQUALIZE_HISTOGRAMM;
                    Properties.Settings.Default.EQUALIZE_HISTOGRAMM = true;
                }
                if (chbItem.Equals("ADAPTIVE_THRESOLD"))
                {
                    preprocessings += (int) Preprocessings.ADAPTIVE_THRESOLD;
                    Properties.Settings.Default.ADAPTIVE_THRESOLD = true;
                }
                if (chbItem.Equals("THRESOLD"))
                {
                    preprocessings += (int) Preprocessings.THRESOLD;
                    Properties.Settings.Default.THRESOLD = true;
                }
                if (chbItem.Equals("PYRAMIDAL_ADAPTIVE"))
                {
                    preprocessings += (int) Preprocessings.PYRAMIDAL_ADAPTIVE;
                    Properties.Settings.Default.PYRAMIDAL_ADAPTIVE = true;
                }
            }
            Properties.Settings.Default.PREPROCESSINGS = preprocessings;
        }

        private void applyFields()
        {
            Int32 tmp = 0;

            Properties.Settings.Default.BINARISATION_TRH = Int32.TryParse(BINARISATION_TRH.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BINARISATION_TRH;
            Properties.Settings.Default.BINARISATION_MAX = Int32.TryParse(BINARISATION_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BINARISATION_MAX;
            Properties.Settings.Default.BLUR_KERNEL_W = Int32.TryParse(BLUR_KERNEL_W.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BLUR_KERNEL_W;
            Properties.Settings.Default.BLUR_KERNEL_H = Int32.TryParse(BLUR_KERNEL_H.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BLUR_KERNEL_H;
            Properties.Settings.Default.ADAPTIVE_TRH_MAX = Int32.TryParse(ADAPTIVE_TRH_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_MAX;
            Properties.Settings.Default.ADAPTIVE_TRH_BLOCK = Int32.TryParse(ADAPTIVE_TRH_BLOCK.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_BLOCK;
            Properties.Settings.Default.ADAPTIVE_TRH_C = Int32.TryParse(ADAPTIVE_TRH_C.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_C;
            Properties.Settings.Default.KUWAHARA_KERNEL = Int32.TryParse(KUWAHARA_KERNEL.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.KUWAHARA_KERNEL;
            Properties.Settings.Default.MIN_HESSIAN = Int32.TryParse(MIN_HESSIAN.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.MIN_HESSIAN;
            Properties.Settings.Default.PYR_NOIZE_TRH = Int32.TryParse(PYR_NOIZE_TRH.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_NOIZE_TRH;
            Properties.Settings.Default.PYR_LEVEL = Int32.TryParse(PYR_LEVEL.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_LEVEL;
            Properties.Settings.Default.PYR_MIN = Int32.TryParse(PYR_MIN.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_MIN;
            Properties.Settings.Default.PYR_MAX = Int32.TryParse(PYR_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_MAX;
            Properties.Settings.Default.PYR_K = Int32.TryParse(PYR_K.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_K;
        }
    }
}
