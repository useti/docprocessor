﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessingWrap;

namespace TestApp
{
    public partial class MainForm : Form
    {
        private int childFormNumber = 0;

        public MainForm()
        {
            string appPath = Application.StartupPath;
            Processing prc = new Processing();
            prc.SetDDirectory(appPath);

            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fname = saveFileDialog1.FileName;
                string template = Properties.Settings.Default.CaseTemplate;

                string case_template = "{" + string.Format(
                    template,
                    //"INPUT_FILE":"{0}",  
                    "%INPUT_FILE%",
                    //"OUTPUT_FILE":"{1}", 
                    "%OUTPUT_FILE%",
                    //"TEMPLATE_FILE":"{2}", 
                    "%TEMPLATE_FILE%",
                    //"PREPROCESSINGS": {3}, 
                    Properties.Settings.Default.PREPROCESSINGS,
                    //"BINARISATION_TRH": {4}, 
                    Properties.Settings.Default.BINARISATION_TRH,
                    //"BINARISATION_MAX": {5}, 
                    Properties.Settings.Default.BINARISATION_MAX,
                    //"BLUR_KERNEL_W": {6},
                    Properties.Settings.Default.BLUR_KERNEL_W,
                    //"BLUR_KERNEL_H": {7}, 
                    Properties.Settings.Default.BLUR_KERNEL_H,
                    //"ADAPTIVE_TRH_MAX": {8},
                    Properties.Settings.Default.ADAPTIVE_TRH_MAX,
                    //"ADAPTIVE_TRH_BLOCK": {9}, 
                    Properties.Settings.Default.ADAPTIVE_TRH_BLOCK,
                    //"ADAPTIVE_TRH_C": {10},
                    Properties.Settings.Default.ADAPTIVE_TRH_C,
                    //"KUWAHARA_KERNEL": {11}, 
                    Properties.Settings.Default.KUWAHARA_KERNEL,
                    //"MIN_HESSIAN": {12},
                    Properties.Settings.Default.MIN_HESSIAN,
                    //"PYR_NOIZE_TRH": {13}, 
                    Properties.Settings.Default.PYR_NOIZE_TRH,
                    //"PYR_LEVEL": {14}, 
                    Properties.Settings.Default.PYR_LEVEL,
                    //"PYR_MIN": {15}, 
                    Properties.Settings.Default.PYR_MIN,
                    //"PYR_MAX": {16},
                    Properties.Settings.Default.PYR_MAX,
                    //"PYR_K": {17},
                    Properties.Settings.Default.PYR_K,
                    //"CASE_NAME" : "{18}"
                    ""
                    ) + "}";

                Case c = new Case(case_template, "case-" + childFormNumber++, fname)
                {
                    MdiParent = this
                };
                c.Show();
            }
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Case Files (*.case)|*.case";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string fname = openFileDialog.FileName;

                string case_template = File.ReadAllText(fname);

                Case c = new Case(case_template, fname)
                {
                    MdiParent = this
                };
                c.Show();
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Case Files (*.case)|*.case";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;

                Case c = (Case) ActiveMdiChild;
                c.Save(fileName);
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }

        private void toolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        public void SetStatus(string statusText)
        {
            toolStripStatusLabel.Text = statusText;
        }
    }
}
