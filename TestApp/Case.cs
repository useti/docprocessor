﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProcessingWrap;

namespace TestApp
{
    public partial class Case : Form
    {
        private string _description;
        private string _name;
        private string _filename = "";

        private JObject _caseJson;
        private bool _isSaved = false;

        public bool IsSaved
        {
            get { return _isSaved; }
            private set { _isSaved = value; }
        }

        public JObject caseJson
        {
            get { return _caseJson; }
            private set
            {
                _caseJson = value;
                IsSaved = false;
            }
        }

        private void FillPreprocessings()
        {
            chlbPreprocessings.Items.Add(Preprocessings.NO_PREPROCESS.ToString(),
                Properties.Settings.Default.NO_PREPROCESS);
            chlbPreprocessings.Items.Add(Preprocessings.BLUR_IMG.ToString(), Properties.Settings.Default.BLUR_IMG);
            chlbPreprocessings.Items.Add(Preprocessings.KUWAHARA_BLUR.ToString(),
                Properties.Settings.Default.KUWAHARA_BLUR);
            chlbPreprocessings.Items.Add(Preprocessings.EQUALIZE_HISTOGRAMM.ToString(),
                Properties.Settings.Default.EQUALIZE_HISTOGRAMM);
            chlbPreprocessings.Items.Add(Preprocessings.ADAPTIVE_THRESOLD.ToString(),
                Properties.Settings.Default.ADAPTIVE_THRESOLD);
            chlbPreprocessings.Items.Add(Preprocessings.THRESOLD.ToString(), Properties.Settings.Default.THRESOLD);
            chlbPreprocessings.Items.Add(Preprocessings.PYRAMIDAL_ADAPTIVE.ToString(),
                Properties.Settings.Default.PYRAMIDAL_ADAPTIVE);
        }
    
        public Case(String caseTemplate, String caseName, String fileName)
        {
            Description = caseTemplate;

            InitializeComponent();
            
            Name = caseName;
            _filename = fileName;

            FillPreprocessings();

            FillFields();
        }


        public Case(String caseTemplate, String fileName)
        {
            Description = caseTemplate;

            InitializeComponent();

            Name = caseJson["CASE_NAME"].Value<String>();

            _filename = fileName;

            FillPreprocessings();

            FillFields();
        }

        private void FillFields()
        {
            BINARISATION_TRH.Text = caseJson["BINARISATION_TRH"].Value<Int32>().ToString();
            BINARISATION_MAX.Text = caseJson["BINARISATION_MAX"].Value<Int32>().ToString();
            BLUR_KERNEL_W.Text = caseJson["BLUR_KERNEL_W"].Value<Int32>().ToString();
            BLUR_KERNEL_H.Text = caseJson["BLUR_KERNEL_H"].Value<Int32>().ToString();
            ADAPTIVE_TRH_MAX.Text = caseJson["ADAPTIVE_TRH_MAX"].Value<Int32>().ToString();
            ADAPTIVE_TRH_BLOCK.Text = caseJson["ADAPTIVE_TRH_BLOCK"].Value<Int32>().ToString();
            ADAPTIVE_TRH_C.Text = caseJson["ADAPTIVE_TRH_C"].Value<Int32>().ToString();
            KUWAHARA_KERNEL.Text = caseJson["KUWAHARA_KERNEL"].Value<Int32>().ToString();
            MIN_HESSIAN.Text = caseJson["MIN_HESSIAN"].Value<Int32>().ToString();
            PYR_NOIZE_TRH.Text = caseJson["PYR_NOIZE_TRH"].Value<Int32>().ToString();
            PYR_LEVEL.Text = caseJson["PYR_LEVEL"].Value<Int32>().ToString();
            PYR_MIN.Text = caseJson["PYR_MIN"].Value<Int32>().ToString();
            PYR_MAX.Text = caseJson["PYR_MAX"].Value<Int32>().ToString();
            PYR_K.Text = caseJson["PYR_K"].Value<Int32>().ToString();

            string template_file = caseJson["TEMPLATE_FILE"].Value<String>();
            if ((!template_file.Equals("%TEMPLATE_FILE%")) && (template_file.Length != 0))
            {
                ShowTemplate(template_file.Replace("\\\\","\\"));
            }

            string input_file = caseJson["INPUT_FILE"].Value<String>();
            if ((!input_file.Equals("%INPUT_FILE%")) && (input_file.Length != 0))
            {
                ShowDocument(input_file.Replace("\\\\", "\\"));
            }

            string output_file = caseJson["OUTPUT_FILE"].Value<String>();
            if ((!output_file.Equals("%OUTPUT_FILE%")) && (output_file.Length != 0))
            {
                ShowOutput(output_file.Replace("\\\\", "\\"));
            }
        }

        public string Description
        {
            get { return _description; }
            private set
            {
                _description = value;
                caseJson = JObject.Parse(_description);
                IsSaved = false;
            }
        }

        public string Name
        {
            get { return _name; }
            private set
            {
                _name = value;
                CASE_NAME.Text = _name;
                Text = _name;
                caseJson["CASE_NAME"] = _name;
                IsSaved = false;
            }
        }

        public string FileName
        {
            get { return _filename; }
            private set
            {
                _filename = value;
                IsSaved = false;
            }
        }

        public void Save(String filename)
        {
            _filename = filename;

            applyPreprocessings();
            applyFields();

            _description = caseJson.ToString();

            File.WriteAllText(FileName, caseJson.ToString());
            IsSaved = true;
        }

        private void CASE_NAME_TextChanged(object sender, EventArgs e)
        {
            Name = CASE_NAME.Text;
        }

        private void applyFields()
        {
            Int32 tmp = 0;

            caseJson["INPUT_FILE"] = tbDocument.Text.Replace("\\", "\\\\");
            caseJson["OUTPUT_FILE"] = tbOutput.Text.Replace("\\", "\\\\");
            caseJson["TEMPLATE_FILE"] = tbTemplate.Text.Replace("\\","\\\\");

            caseJson["BINARISATION_TRH"] = Int32.TryParse(BINARISATION_TRH.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BINARISATION_TRH;
            caseJson["BINARISATION_MAX"] = Int32.TryParse(BINARISATION_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BINARISATION_MAX;
            caseJson["BLUR_KERNEL_W"] = Int32.TryParse(BLUR_KERNEL_W.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BLUR_KERNEL_W;
            caseJson["BLUR_KERNEL_H"] = Int32.TryParse(BLUR_KERNEL_H.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.BLUR_KERNEL_H;
            caseJson["ADAPTIVE_TRH_MAX"] = Int32.TryParse(ADAPTIVE_TRH_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_MAX;
            caseJson["ADAPTIVE_TRH_BLOCK"] = Int32.TryParse(ADAPTIVE_TRH_BLOCK.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_BLOCK;
            caseJson["ADAPTIVE_TRH_C"] = Int32.TryParse(ADAPTIVE_TRH_C.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.ADAPTIVE_TRH_C;
            caseJson["KUWAHARA_KERNEL"] = Int32.TryParse(KUWAHARA_KERNEL.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.KUWAHARA_KERNEL;
            caseJson["MIN_HESSIAN"] = Int32.TryParse(MIN_HESSIAN.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.MIN_HESSIAN;
            caseJson["PYR_NOIZE_TRH"] = Int32.TryParse(PYR_NOIZE_TRH.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_NOIZE_TRH;
            caseJson["PYR_LEVEL"] = Int32.TryParse(PYR_LEVEL.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_LEVEL;
            caseJson["PYR_MIN"] = Int32.TryParse(PYR_MIN.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_MIN;
            caseJson["PYR_MAX"] = Int32.TryParse(PYR_MAX.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_MAX;
            caseJson["PYR_K"] = Int32.TryParse(PYR_K.Text, out tmp)
                ? tmp
                : Properties.Settings.Default.PYR_K;
        }
    

        private void applyPreprocessings()
        {
            int preprocessings = 0;
            foreach (var chbItem in chlbPreprocessings.CheckedItems)
            {
                if (chbItem.Equals("BLUR_IMG"))
                {
                    preprocessings += (int)Preprocessings.BLUR_IMG;
                }
                if (chbItem.Equals("KUWAHARA_BLUR"))
                {
                    preprocessings += (int)Preprocessings.KUWAHARA_BLUR;
                }
                if (chbItem.Equals("EQUALIZE_HISTOGRAMM"))
                {
                    preprocessings += (int)Preprocessings.EQUALIZE_HISTOGRAMM;
                }
                if (chbItem.Equals("ADAPTIVE_THRESOLD"))
                {
                    preprocessings += (int)Preprocessings.ADAPTIVE_THRESOLD;
                }
                if (chbItem.Equals("THRESOLD"))
                {
                    preprocessings += (int)Preprocessings.THRESOLD;
                }
                if (chbItem.Equals("PYRAMIDAL_ADAPTIVE"))
                {
                    preprocessings += (int)Preprocessings.PYRAMIDAL_ADAPTIVE;
                }
            }
            caseJson["PREPROCESSINGS"] = preprocessings;
            
        }


        private void btnProcess_Click(object sender, EventArgs e)
        {
            applyPreprocessings();
            applyFields();

            _description = caseJson.ToString();

            Processing prc = new Processing();

            int result  = prc.ProcessImages(_description);
            RunStatus rs = (RunStatus) result;


            MainForm mf = (MainForm) MdiParent;
            mf.SetStatus(rs.ToString());

            if (File.Exists(tbOutput.Text))
            {
                ShowOutput(tbOutput.Text);
            }

        }

        private void chlbPreprocessings_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                for (int i = 1; i < chlbPreprocessings.Items.Count; i++)
                {
                    chlbPreprocessings.SetItemChecked(i, false);
                }
            }
        }

        private void btnOpenTemplate_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string template_file = openFileDialog1.FileName;

                ShowTemplate(template_file);
            }
        }

        private void ShowTemplate(string template_file)
        {
            tbTemplate.Text = template_file;

            pbTemplate.SizeMode = PictureBoxSizeMode.StretchImage;
            Bitmap MyImage = new Bitmap(template_file);
            pbTemplate.Image = (Image) MyImage;

            tabControl1.SelectedIndex = 0;
        }

        private void btnOpenDoc_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string doc_file = openFileDialog1.FileName;

                ShowDocument(doc_file);
            }
        }

        private void ShowDocument(string doc_file)
        {
            tbDocument.Text = doc_file;

            pbDocument.SizeMode = PictureBoxSizeMode.StretchImage;
            Bitmap MyImage = new Bitmap(doc_file);
            pbDocument.Image = (Image) MyImage;

            tabControl1.SelectedIndex = 1;
        }

        private void btnSavePath_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string out_file = saveFileDialog1.FileName;

                ShowOutput(out_file);
            }
        }

        private void ShowOutput(string out_file)
        {
            tbOutput.Text = out_file;

            if (File.Exists(out_file))
            {
                pbOutput.SizeMode = PictureBoxSizeMode.StretchImage;
                Bitmap MyImage = new Bitmap(out_file);
                pbOutput.Image = (Image) MyImage;
            }
            tabControl1.SelectedIndex = 2;
        }

        private void tbTemplate_TextChanged(object sender, EventArgs e)
        {
            IsSaved = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save(FileName);
        }
    }
}
