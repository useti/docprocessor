#pragma once
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include <opencv2/imgproc.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include"opencv2/xfeatures2d/nonfree.hpp"

#include "Settings.h"

#define PR_DLL__OK			0
#define PR_DLL_ERR			1
#define PR_DLL_LOAD_IMG_ERR	2
#define PR_DLL_CLEANUP_ERR	3
#define PR_DLL_RESIZE_ERR	4
#define PR_DLL_EXTRACT_ERR	5
#define PR_DLL_SAVE_IMG_ERR	16


#define PR_DLL_PREPR_NOPR 0
#define PR_DLL_PREPR_BLUR 1
#define PR_DLL_PREPR_KUWH 2
#define PR_DLL_PREPR_EQHS 4
#define PR_DLL_PREPR_ATRH 8
#define PR_DLL_PREPR_TRHS 16
#define PR_DLL_PREPR_APYR 32

#define KUWAHARA_KERNEL 7

#define MOW 3

#define SHOW_DEBUG_IMG(window_name,ima) \
	imshow(static_cast<char*>( window_name ), ima ); \
	cv::waitKey(0); \
	cv::destroyWindow(static_cast<char*>( window_name ));

struct preprocess {
	unsigned char blur		: 1;  // 0000000?
	unsigned char kuwahara	: 1;  // 000000?0
	unsigned char eq_ist	: 1;  // 00000?00
	unsigned char athrsld	: 1;  // 0000?000 
	unsigned char thrsld	: 1;  // 000?0000 
	unsigned char apyramid  : 1;  // 00?00000 
};

extern "C" __declspec(dllexport)
int SubstractTemplate(const char* settings_c_str);