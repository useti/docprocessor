#include "ProcessingDLL.h"
#include <iostream>

using namespace cv;

Mat PiramidalAdaptive(Mat input_image, double noize_trh, size_t final_level, int min_value = 0, int max_value = 255 ,int k = 1)
{
	Mat ret(input_image);

	// Build piramidal description
	std::vector<Mat> avg_masks;
	std::vector<Mat> min_masks;
	std::vector<Mat> max_masks;
	std::vector<Mat> trh_masks;
	std::vector<Mat> masks;

	size_t mask_rows = input_image.rows / 2;
	size_t mask_cols = input_image.cols / 2;

	Mat layer(input_image);

	while ((mask_cols > 1) && (mask_rows > 1))
	{
		Mat avg_mask = Mat::zeros(mask_rows, mask_cols, CV_8U);
		Mat min_mask = Mat::zeros(mask_rows, mask_cols, CV_8U);
		Mat max_mask = Mat::zeros(mask_rows, mask_cols, CV_8U);
		Mat prev_layer(layer);

		resize(layer, layer, Size(mask_cols, mask_rows), CV_INTER_CUBIC);
		//resize(layer, layer, Size(mask_cols, mask_rows));
		double min = 0;
		double max = 0;
		double avg = 0;


		for (size_t row = 0; row < mask_rows; ++row)
		{
			for (size_t col = 0; col < mask_cols; ++col)
			{
				Mat window = prev_layer.colRange(col * 2, col * 2 + 2).rowRange(row * 2, row * 2 + 2);
				minMaxIdx(window, &min, &max);
				avg = mean(window)[0];
				avg_mask.at<unsigned char>(row, col) = static_cast<unsigned char>(cvRound(avg));
				min_mask.at<unsigned char>(row, col) = static_cast<unsigned char>(cvRound(min));
				max_mask.at<unsigned char>(row, col) = static_cast<unsigned char>(cvRound(max));
			}
		}

		avg_masks.push_back(avg_mask);
		min_masks.push_back(min_mask);
		max_masks.push_back(max_mask);
		masks.push_back(layer);

		mask_cols /= 2;
		mask_rows /= 2;
	}

	size_t num_masks = avg_masks.size();

	if (num_masks > 2){
		mask_cols = layer.cols;
		mask_rows = layer.rows;

		Mat mask(avg_masks[num_masks - 1]);

		for (int i = num_masks - 2; i >= 0; i--)
		{
			
			Mat avg_mask = avg_masks[i];
			Mat min_mask = min_masks[i];
			Mat max_mask = max_masks[i];
			layer = masks[i];
			mask_cols = layer.cols;
			mask_rows = layer.rows;

			resize(mask, mask, Size(mask_cols, mask_rows), CV_INTER_CUBIC);
			//resize(mask, mask, Size(mask_cols, mask_rows));
			

			for (size_t row = 0; row < mask_rows; ++row)
			{
				for (size_t col = 0; col < mask_cols; ++col)
				{
					double avg = avg_mask.at<unsigned char>(row, col);
					double min = min_mask.at<unsigned char>(row, col);
					double max = max_mask.at<unsigned char>(row, col);
					

					double diff = max - min;

					if (diff > noize_trh)
					{
						double trh_a = avg;
						double trh_b = (max - min) / 2.;

						double trh = trh_a > trh_b ? trh_a : trh_b;

						mask.at<unsigned char>(row, col) = static_cast<unsigned char>(cvRound(trh));
					}
				}
			}
			masks[i] = mask;
		}

		mask = masks[final_level];
		resize(mask, mask, Size(ret.cols, ret.rows), CV_INTER_CUBIC);
		//resize(mask, mask, Size(ret.cols, ret.rows));
		mask_cols = ret.cols;
		mask_rows = ret.rows;
#ifdef _DEBUG
		SHOW_DEBUG_IMG("mask", mask);
#endif
		for (size_t row = 0; row < mask_rows; ++row)
		{
			for (size_t col = 0; col < mask_cols; ++col)
			{
				unsigned char value = ret.at<unsigned char>(row, col);
				unsigned char trh = mask.at<unsigned char>(row, col);
				if (k == 1)
				{
					ret.at<unsigned char>(row, col) = value >= trh ? max_value : min_value;
				} else
				{
					int pixel = k*(value - trh) + trh;
					if (pixel < 0)
						pixel *= -1;
					if (pixel >= max_value)
						pixel = max_value;
					if (pixel <= min_value)
						pixel = min_value;
					ret.at<unsigned char>(row, col) = static_cast<unsigned char>(pixel);
				}
				
			}
		}

	}
	return ret;
}

Mat kuwahara_gray(Mat img, int kernel_size)
{
	int t = img.type();
	int nchannels = img.channels();
	int w = img.cols;
	int h = img.rows;
	Mat out(h, w, t);

	int dt = kernel_size / 2;
	{
		for (int y = 2 * dt; y <h - 2 * dt; ++y)
		{
			uchar* ptr = static_cast<uchar*>(img.data + y*img.step);
			uchar* p_out = static_cast<uchar*>(out.data + y*out.step);
			for (int x = 2 * dt; x <w - 2 * dt; ++x)
			{
				int gr;
				int sum;
				double mean1;
				double mean2;
				double mean3;
				double mean4;
				double disp1;
				double disp2;
				double disp3;
				double disp4;

				int k = 0;
				//1
				sum = 0;
				mean1 = 0;
				uchar* pd = ptr;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						sum += pd[x + k2];
						++k;
					}
					pd += img.step;
				}
				mean1 = sum / ((dt + 1)*(dt + 1));

				pd = ptr;
				disp1 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						disp1 += (pd[x + k2] - mean1)*(pd[x + k2] - mean1);
					}
					pd += img.step;
				}

				//2
				sum = 0;
				mean2 = 0;
				pd = ptr + dt*img.step;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean2 = sum / ((dt + 1)*(dt + 1));

				pd = ptr + dt*img.step;
				disp2 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						disp2 += (pd[x + k2] - mean2)*(pd[x + k2] - mean2);
					}
					pd += img.step;
				}

				//3
				sum = 0;
				mean3 = 0;
				pd = ptr;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean3 = sum / ((dt + 1)*(dt + 1));

				pd = ptr;
				disp3 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						disp3 += (pd[x + k2] - mean3)*(pd[x + k2] - mean3);
					}
					pd += img.step;
				}

				//4
				sum = 0;
				mean4 = 0;
				pd = ptr + dt*img.step;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean4 = sum / ((dt + 1)*(dt + 1));

				pd = ptr + dt*img.step;
				disp4 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						disp4 += (pd[x + k2] - mean4)*(pd[x + k2] - mean4);
					}
					pd += img.step;
				}

				gr = 0;
				if ((disp1 <= disp2) && (disp1 <= disp3) && (disp1 <= disp4))
					gr = mean1;
				if ((disp2 <= disp1) && (disp2 <= disp3) && (disp2 <= disp4))
					gr = mean2;
				if ((disp3 <= disp1) && (disp3 <= disp2) && (disp3 <= disp4))
					gr = mean3;
				if ((disp4 <= disp1) && (disp4 <= disp2) && (disp4 <= disp3))
					gr = mean4;

				p_out[x] = gr;
			}
		}
	}
	return out;
}

Mat Cleanup(Mat input_image, Settings extract_settings)
{
	Mat ret;
	preprocess* prpr = reinterpret_cast<preprocess*>(&extract_settings.preprocessings);
	input_image.copyTo(ret);
#ifdef _DEBUG
	SHOW_DEBUG_IMG("grayscale", ret);
#endif
	
	if (prpr->apyramid > 0)
	{
		Mat apr = PiramidalAdaptive(
			ret,
			extract_settings.pyrNoizeTrh,
			extract_settings.pyrLevel,
			extract_settings.pyrMin,
			extract_settings.pyrMax,
			extract_settings.pyrK);
		apr.copyTo(ret);
#ifdef _DEBUG
		SHOW_DEBUG_IMG("apyramid", ret);
#endif
	}

	if (prpr->eq_ist > 0)
	{
		Mat eq(ret);
		equalizeHist(eq, ret);
#ifdef _DEBUG
		SHOW_DEBUG_IMG("eqhist", ret);
#endif
	}
	
	if (prpr->blur > 0)
	{
		Mat blr(ret);
		blur(blr, ret, Size(extract_settings.blurKernelW, extract_settings.blurKernelW));
#ifdef _DEBUG
		SHOW_DEBUG_IMG("blur", ret);
#endif
	}

	if (prpr->thrsld > 0)
	{
		Mat trh(ret);
		threshold(trh, ret, extract_settings.binarisationThresold, extract_settings.binarisationMax, CV_THRESH_BINARY);
#ifdef _DEBUG
		SHOW_DEBUG_IMG("thresold", ret);
#endif
	}

	if (prpr->athrsld > 0)
	{
		Mat atrh(ret);
		adaptiveThreshold(
			atrh, ret,
			extract_settings.adaptivThrMax,
			CV_ADAPTIVE_THRESH_GAUSSIAN_C,
			CV_THRESH_BINARY,
			extract_settings.adaptivThrBlock,
			extract_settings.adaptivThrC);
#ifdef _DEBUG
		SHOW_DEBUG_IMG("adaptive thresold", ret);
#endif
	}
	
	if (prpr->kuwahara > 0)
	{
		Mat kuwahara = kuwahara_gray(ret, extract_settings.kuwaharaKernel);
		kuwahara.copyTo(ret);
#ifdef _DEBUG
		SHOW_DEBUG_IMG("kuwahara", ret);
#endif
	}
	
	return ret;
}

Mat MatchSize(Mat img_scene, Mat img_object, Settings extract_settings)
{
	int minHessian = extract_settings.minHessian;

	Ptr<xfeatures2d::SURF> detector = xfeatures2d::SURF::create(minHessian);

	std::vector<KeyPoint> keypoints_object, keypoints_scene;

	detector->detect(img_object, keypoints_object);
	detector->detect(img_scene, keypoints_scene);

	Mat descriptors_object, descriptors_scene;

	detector->compute(img_object, keypoints_object, descriptors_object);
	detector->compute(img_scene, keypoints_scene, descriptors_scene);

	FlannBasedMatcher matcher;
	std::vector< DMatch > matches;
	matcher.match(descriptors_object, descriptors_scene, matches);

	double max_dist = 0; double min_dist = 100;
	double avg_dist = 0;

	for (int i = 0; i < descriptors_object.rows; i++)
	{
		
		double dist = matches[i].distance;
		avg_dist += dist;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}
	avg_dist = avg_dist / static_cast<double>(matches.size());

	std::vector< DMatch > good_matches;

	for (int i = 0; i < descriptors_object.rows; i++)
	{
		if ((matches[i].distance < avg_dist / 2) || (matches[i].distance == 0))
		{
			good_matches.push_back(matches[i]);
		}
	}

	std::vector<Point2f> obj;
	std::vector<Point2f> scene;

	for (int i = 0; i < good_matches.size(); i++)
	{
		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
	}

	Mat H = findHomography(obj, scene, CV_RANSAC);
	Mat H1 = findHomography(scene,obj, CV_RANSAC);

	std::vector<Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(img_object.cols, 0);
	obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);
	
	std::vector<Point2f> scene_corners(4);
	std::vector<Point2f> back_corners(4);

	perspectiveTransform(obj_corners, scene_corners, H);
	perspectiveTransform(scene_corners, back_corners, H1);

	Mat realigned;

	warpPerspective(img_scene, realigned, H1, Size(img_object.cols, img_object.rows));

#ifdef _DEBUG
	SHOW_DEBUG_IMG("realigned", realigned);
	
	Mat img_matches;
	drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
	line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);

	SHOW_DEBUG_IMG("matches", img_matches);
#endif

	return realigned;
}

int SubstractTemplate(const char* settings_c_str)
{
	try{
		std::string settings_string = settings_c_str;
		Settings extcract_settings(settings_string);

		Mat input_image, input_clean;
		Mat template_image, template_clean;
		Mat output_image, ret;

		// STEP 0 load 
		try
		{
			input_image = imread(extcract_settings.inputFile.c_str());
			template_image = imread(extcract_settings.templateFile.c_str());
		}
		catch (...)
		{
			return PR_DLL_LOAD_IMG_ERR;
		}

		// STEP 2 match size
		try
		{

			cvtColor(input_image, input_clean, CV_BGR2GRAY);
			cvtColor(template_image, template_clean, CV_BGR2GRAY);
			input_clean = MatchSize(input_clean, template_clean, extcract_settings);
		}
		catch (...)
		{
			return PR_DLL_RESIZE_ERR;
		}

		// STEP 3 cleanup images (template and filled document)
		try
		{
			// We do same preprocessings for template and document
			input_clean = Cleanup(input_clean, extcract_settings);
			template_clean = Cleanup(template_clean, extcract_settings);
		}
		catch (...)
		{
			return PR_DLL_CLEANUP_ERR;
		}
		
		// STEP 4 extract template from document
		try
		{
			input_clean.copyTo(output_image);

			Mat mask(template_clean);
			threshold(mask, mask, extcract_settings.binarisationThresold, extcract_settings.binarisationMax, CV_THRESH_BINARY_INV);
#ifdef _DEBUG
			SHOW_DEBUG_IMG("mask", mask);
#endif
			Mat blank(mask.rows, mask.cols, mask.type(), Scalar(255));
			blank.copyTo(output_image, mask);

			// final cleanup
			output_image = Cleanup(output_image, extcract_settings);

		} catch (...)
		{
			return PR_DLL_EXTRACT_ERR;
		}
		
		// FINAL step - write output file
		try
		{
			imwrite(extcract_settings.outputFile.c_str(), output_image);
#ifdef _DEBUG
			SHOW_DEBUG_IMG("output", output_image);
#endif
		} catch (...)
		{
			return PR_DLL_SAVE_IMG_ERR;
		}
	}
	catch (Exception ex)
	{
		return PR_DLL_ERR;
	}
	return PR_DLL__OK;
}
