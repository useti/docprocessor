#include "Settings.h"

Settings::Settings(std::string settings_string)
{
	Json::Features features;
	Json::Value root;

	Json::Reader reader(features);

	if (reader.parse(settings_string, root))
	{
		inputFile = root[static_cast<char*>("INPUT_FILE")].asString();
		outputFile = root[static_cast<char*>("OUTPUT_FILE")].asString();
		templateFile = root[static_cast<char*>("TEMPLATE_FILE")].asString();
		preprocessings = static_cast<unsigned char>(root[static_cast<char*>("PREPROCESSINGS")].asUInt());
		binarisationThresold = root[static_cast<char*>("BINARISATION_TRH")].asInt();
		binarisationMax = root[static_cast<char*>("BINARISATION_MAX")].asInt();
		blurKernelW = root[static_cast<char*>("BLUR_KERNEL_W")].asInt();
		blurKernelH = root[static_cast<char*>("BLUR_KERNEL_H")].asInt();
		adaptivThrMax = root[static_cast<char*>("ADAPTIVE_TRH_MAX")].asInt();
		adaptivThrBlock = root[static_cast<char*>("ADAPTIVE_TRH_BLOCK")].asInt();
		adaptivThrC = root[static_cast<char*>("ADAPTIVE_TRH_C")].asInt();
		kuwaharaKernel = root[static_cast<char*>("KUWAHARA_KERNEL")].asInt();
		minHessian = root[static_cast<char*>("MIN_HESSIAN")].asInt();
		pyrNoizeTrh = root[static_cast<char*>("PYR_NOIZE_TRH")].asInt();
		pyrLevel = root[static_cast<char*>("PYR_LEVEL")].asInt();
		pyrMin = root[static_cast<char*>("PYR_MIN")].asInt();
		pyrMax = root[static_cast<char*>("PYR_MAX")].asInt();
		pyrK = root[static_cast<char*>("PYR_K")].asInt();
	}
}

Settings::~Settings()
{
}
