#pragma once
#include <string>
#include "json/json.h"
#include "json/value.h"


class Settings
{
public:
	Settings(std::string settings_string);
	virtual ~Settings();
	std::string inputFile;
	std::string outputFile;
	std::string templateFile;
	unsigned char preprocessings;
	int binarisationThresold;
	int binarisationMax;
	int blurKernelW;
	int blurKernelH;
	int adaptivThrMax;
	int adaptivThrBlock;
	int adaptivThrC;
	int kuwaharaKernel;
	int minHessian;
	int pyrNoizeTrh;
	int pyrLevel;
	int pyrMin;
	int pyrMax;
	int pyrK;
};

